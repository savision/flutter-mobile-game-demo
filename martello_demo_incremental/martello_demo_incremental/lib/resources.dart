import 'package:martello_demo_incremental/http.dart';

enum currency {
  Money,
  Jobs,
  TechJobs,
  TechJobsAtMartello,
  MoonBases,
  MartianRovers,
  MartianBases,
  JobMachines,
}

class CurrencyManager {
  static Map<currency, CurrencyManager> datam;
  List<CurrencyManager> upgrades = List<CurrencyManager>();

  double count = 0;
  double incomePer = 0;
  double costPer = 10;
  double costMult = 1.15;
  String name = "";
  String descriptionHelper = "";
  currency produces;
  currency costs;

  void update() {
    datam[produces].count += incomePer * count / 10;
  }

  bool canBuyAnother() {
    return datam[costs].count >= costPer;
  }

  bool buyAnother() {
    if (datam[costs].count >= costPer) {
      datam[costs].count -= costPer;
      count++;
      costPer = (costPer * costMult).floorToDouble();
      return true;
    }
    return false;
  }

  // Used for applying upgrades to currencies.
  Function(CurrencyManager) apply = (f) {};

  String title() {
    return count.toStringAsFixed(0) +
        " " +
        name +
        " each earning " +
        (incomePer).toStringAsFixed(1) +
        " " +
        costs.toString().substring(9) +
        " per second";
  }

  String description() {
    return "$descriptionHelper \n${(incomePer * count).toStringAsFixed(1)} ${produces.toString().substring(9)} per second total";
  }

  String costOf() {
    return "Buy For\n${costPer.toStringAsFixed(0)}\n${costs.toString().substring(9)}";
  }
}

Map<currency, CurrencyManager> generateResources() {
  Map<currency, CurrencyManager> datam = Map<currency, CurrencyManager>();

  double pMult = 100;
  double pCurrent = 10 / pMult;
  double iCurrent = 1;
  currency prev = currency.Money;

  currency.values.forEach((r) {
    iCurrent = 0.1 + (prev.index % 3) * (prev.index % 3) * 4.9;

    var iCurrentSnappyShot = iCurrent;
    datam.putIfAbsent(
        r,
        () => CurrencyManager()
          ..costs = currency.Money // .. is the curry operator, for applying a statement and then returning the
          ..name = r.toString().substring(9) // object we were working on. this lets us chain operators forever
          ..produces = currency.values[(prev.index / 3).floor()]
          ..costPer = pCurrent
          ..incomePer = iCurrent
          ..upgrades.add(CurrencyManager()
            ..name = "Income +${iCurrent.toStringAsFixed(1)}"
            ..costs = r
            ..costPer = 10
            ..apply = (f) {
              f.incomePer +=
                  iCurrentSnappyShot; // This occurs later, so it has to b snapshotted
              Database.setValue(f.name + f.name,
                  f.incomePer); //we write the name twice to prevent collisions with elastic search
            }));
    Database.getValue(datam[r].name + datam[r].name, datam[r].incomePer).then(
        (v) => datam[r].incomePer =
            v); //we write the name twice to prevent collisions with elastic search

    pCurrent *= pMult;
    prev = r;
  });
  datam[currency.Money].incomePer = 0;
  CurrencyManager.datam = datam;
  return datam;
}
