import 'package:http/http.dart' as http;
import 'dart:async';

class Database {
  static const String ENDPOINT =
      "https://search-demo-app-incremental-hkvwbilx6mzxg2a4npfutpfvqq.us-east-2.es.amazonaws.com/";

  static void setValue(String name, double val) {
    final response = http.put(ENDPOINT + "upgrades/_doc/$name",
        body: '{"name":"$name","innervalue":$val}',
        headers: {"Content-Type": "application/json"});
    response.then((x) => print(x.body.toString()));
  }

  static Future<double> getValue(String name, double defaultValue) {
    try {
      final response = http.get(ENDPOINT + "upgrades/_search?q=name:$name",
          headers: {"Content-Type": "application/json"});

      response.then((x) => print(x.body.toString()));

      return response.then((x) {
        var start = x.body.indexOf('"innervalue":') + '"innervalue":'.length;
        var end = x.body.indexOf('}', start);
        print("values[$name,$start,$end]: " + x.body.substring(start, end));
        return double.parse(x.body.substring(start, end));
      });
    } catch (Exception) {
      return Future<double>(() {
        return defaultValue;
      });
    }
  }
}
