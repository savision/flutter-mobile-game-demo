import 'package:flutter/material.dart';
import 'package:martello_demo_incremental/resources.dart';
import 'package:martello_demo_incremental/http.dart';
import 'dart:async';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Martello Incremental Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Martello Incremental Demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Timer _interval;
  var datam = generateResources();
  List<Widget> reses;

  void _incrementCounter() {
    setState(() {
      datam[currency.Money].count += 1;
    });
  }

  void _update(Timer t) {
    setState(() {
      datam.values.forEach((f) => f.update());
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_interval == null) {
      _interval = Timer.periodic(new Duration(milliseconds: 100), _update);
    }

    reses = datam.values.map((f) {
      var buttons = <Widget>[
        RaisedButton(
          color: Colors.blue,
          textColor: Colors.white,
          child: Text( f.costOf(), style: TextStyle(fontSize: 10), textAlign: TextAlign.center,),
          onPressed: f.canBuyAnother() ? (() => setState(() => f.buyAnother())) : null,
        ),
      ];

      f.upgrades.forEach((up) {
        buttons.add(
          RaisedButton(
            color: Colors.blue,
            textColor: Colors.white,
            child: Text(
              "Upgrade ${up.name}\n${up.costPer.toStringAsFixed(1)}\n${up.costs.toString().substring(9)}",
              style: TextStyle(fontSize: 10),
              textAlign: TextAlign.center,
            ),
            onPressed: up.canBuyAnother() ? (() => setState(() {
                      up.buyAnother();
                      up.apply(f);
                    }))
                : null,
          ),
        );
      });

      return ListTile(
        isThreeLine: true,
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Divider(color: Colors.black45, height: 40,),
            Text(f.title()),
          ],
        ),
        subtitle: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(f.description()),
            Spacer(),
            Container(
              width: 100,
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: buttons),
            ),
          ],
        ),
      );
    }).toList();
    
    //Remove money from the list
    reses[0] = ListTile(
        title: Text("Money: " + datam[currency.Money].count.toStringAsFixed(1)));
    
    reses.add(ListTile(
        title: Text(
      "",
      textScaleFactor: 5,
    )));
    
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView(
        children: reses,
      ),
      floatingActionButton: FloatingActionButton(
        highlightElevation: 360,
        backgroundColor: Colors.green,
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Text("+1", textScaleFactor: 2.5, ),
      ),
    );
  }
}
